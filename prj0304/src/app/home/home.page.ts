import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "YzMotos";

  cards = [
  {
    titulo: "NXR 160 Bros ESDD",
    subtitulo: "R$ 15.790",
    conteudo: "Com um novo design mais arrojado, a Bros 160 2022 combina força e modernidade em um modelo único. Ela chegou com um visual mais robusto, com protetor de suspensão e seu clássico motor de 160cc. Ela é a parceria certa para você pilotar com liberdade dentro da cidade ou fora dela[...]",
    foto: "https://http2.mlstatic.com/D_NQ_NP_957012-MLB47526435031_092021-O.jpg"
  },
  {
    titulo:"Honda NC 750X ",
    subtitulo: "R$ 55.700",
    conteudo: "A nova NC 750X oferece, em todas as suas versões, um total de quatro modos de pilotagem, permitindo ao piloto configurar a motocicleta para qualquer tipo de viagem e terreno[...] ",
    foto: "https://www.motoo.com.br/fotos/2022/2/960_720/Honda-NC750X-2022-DCT-11_21022022_48498_960_720.jpg"
  },
  {
    titulo:"CG 160 Start",
    subtitulo:"R$ 11.920",
    conteudo:"A CG 160 Start garante segurança para você encarar o trânsito da cidade com mais tranquilidade. Ela conta com o sistema de freios CBS (Combined Brake System) que distribui a frenagem entre as rodas de maneira inteligente[...]",
    foto: "https://motonewsbrasil.com/wp-content/uploads/2019/08/honda-cg-160-start-2020-1.jpg"
  },
  {
    titulo: "CBR 650R",
    subtitulo: "R$ 50.900",
    conteudo: "Compacta e agressiva, a CBR 650R traz o DNA das pistas e as curvas da CBR 1000RR Fireblade para as ruas. Desde os penetrantes faróis dianteiros em LED, que preenchem a compacta carenagem, até o formato minimalista do escapamento[...]",
    foto: "https://www.rbsdirect.com.br/imagesrc/25728827.jpg?w=700"
  }
  ];
  

  constructor() {}

}
